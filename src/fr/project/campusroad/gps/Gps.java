package fr.project.campusroad.gps;

import fr.project.campusroad.view.MainActivity;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class Gps extends Service implements LocationListener {

	private LocationManager locationManager;
	private MainActivity mMainActivity;

	private boolean isGPSEnabled = false;

	private Location location; // location

	// The minimum distance to change Updates in meters
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 5; // 10 meters

	// The minimum time between updates in milliseconds
	private static final long MIN_TIME_BW_UPDATES = 1000;

	public Gps(MainActivity activity) {
		super();
		mMainActivity = activity;
		initLocation();
	}


	/**
	 * Gps init.
	 * @return
	 */
	public void initLocation() {
		try {
			locationManager = (LocationManager) mMainActivity.getSystemService(LOCATION_SERVICE);

			// getting gps status
			isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
			if( !isGPSEnabled ) return;

			if (location == null) {
				locationManager.requestLocationUpdates(
						LocationManager.GPS_PROVIDER,
						MIN_TIME_BW_UPDATES,
						MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
				Log.d("GPS Enabled", "GPS Enabled");
				if (locationManager != null) {
					location = locationManager
							.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * When the location changed.
	 */
	@Override
	public void onLocationChanged(Location location) {
		mMainActivity.onLocationChanged(location.getLongitude(), location.getLatitude());
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		Toast.makeText(mMainActivity.getApplicationContext(), "Status changed", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onProviderEnabled(String provider) {
		Toast.makeText(mMainActivity.getApplicationContext(), "Gps turned on ", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onProviderDisabled(String provider) {
		Toast.makeText(mMainActivity.getApplicationContext(), "Gps turned off ", Toast.LENGTH_SHORT).show();
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

}
