package fr.project.campusroad.calculs;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import fr.project.campusroad.Geometry.Point;
import fr.project.campusroad.data.Path;
import fr.project.campusroad.data.Position;
import fr.project.campusroad.dataBase.PathBDD;

public class RegisterPath {
	Path currentPath = null;
    List<UserPath> mUserPaths;

    public void setmUserPaths(List<UserPath> mUserPaths) {
        this.mUserPaths = mUserPaths;
    }



	public RegisterPath(){
		currentPath = new Path();
		mUserPaths = new ArrayList<UserPath>();
	}

	public Path getPath(){
		return currentPath;
	}
	
	public void setCurrentPath(int i) {
		currentPath.clean();
		for(Point p : mUserPaths.get(i).getPath().getPoint()) {
			_addPoint(p);
		}
		Log.d("Path", "path="+currentPath.size());
	}

	public String [] getNames() {
		if( mUserPaths.isEmpty()) return null;
		String [] names = new String[mUserPaths.size()];
		int i = 0;
		for(UserPath path : mUserPaths) {
			names[i] = path.getName();
			i++;
		}
		return names;
	}
	
	public void addPoint(Position o) throws Exception{
		throw new Exception("Aucune implem.");
	}

    public void addUserPath(UserPath up) {
        this.mUserPaths.add(up);
    }

	protected void _addPoint(Point p){
		if(currentPath != null){
			currentPath.addPoint(p);
		}
	}

	public void cancel() {
		if(currentPath != null)
			currentPath.clean();
	}

	public void save(String name) {
        mUserPaths.add(new UserPath(name, currentPath));
	}
}
