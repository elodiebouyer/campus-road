package fr.project.campusroad.calculs;

import fr.project.campusroad.Geometry.Point;
import fr.project.campusroad.data.Path;


public class UserPath {
	private String mPathName;
	private Path mPath;
	
	public UserPath(String name, Path path) {
		mPathName = name;
		mPath = new Path();
		for(Point p : path.getPoint()){
			mPath.addPoint(p);
		}
	}
	
	public String getName() {
		return mPathName;
	}
	
	public Path getPath() {
		return mPath;
	}
}
