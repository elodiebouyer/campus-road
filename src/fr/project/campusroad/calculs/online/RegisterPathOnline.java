package fr.project.campusroad.calculs.online;

import android.os.Handler;
import android.os.Message;
import fr.project.campusroad.api.ApiType;
import fr.project.campusroad.api.CallApi;
import fr.project.campusroad.calculs.RegisterPath;
import fr.project.campusroad.data.NewPoint;
import fr.project.campusroad.data.PointPlacement;
import fr.project.campusroad.data.Position;

public class RegisterPathOnline extends RegisterPath {
	
	public RegisterPathOnline(){
		super();
	}

	@Override
	public void addPoint(Position o) {
		Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				NewPoint res = null;
				switch (msg.what) {
				case 3:
					res = (PointPlacement) msg.obj;
					_addPoint(res.getPoint());
					break;
				default:
					break;
				}
			}
		};
		CallApi api;
		try {
			api = new CallApi(handler,CallApi.API_GET, ApiType.API_PLACEMENT, String.valueOf(o.getX()) + ";" + String.valueOf(o.getY()), null);
			api.run();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
