package fr.project.campusroad.api;

public enum ApiType {
	API_BUILDINGS_TYPE,
	API_ROADS_TYPE,
	API_MIN_MAX_TYPE,
	API_PLACEMENT
}
