package fr.project.campusroad.api.parser;

import fr.project.campusroad.data.PointPlacement;
import android.util.JsonReader;
import android.util.JsonToken;

public class ParserPlacement extends JSONParser{
	PointPlacement p = null;

	public ParserPlacement(JsonReader json) {
		super(json);
	}
	
	@Override
	public Object runParsing() throws Exception{
		runParsingObject();
		return p;
	}
	
	@Override
	protected Object parseNextObject() throws Exception {
		PointPlacement placement = new PointPlacement();
		reader.beginObject();
		while(reader.hasNext()){
			placement = (PointPlacement) storeValue(reader.nextName(), placement);
		}
		reader.endObject();
		p = placement;
		return placement;
	}
	
	@Override
	protected Object storeValue(String key, Object plop){
		PointPlacement plopR = (PointPlacement) plop;
		
		try {
			if (reader.peek() == JsonToken.NULL) {
				reader.nextNull();
	            return plopR;
	          }
				if(key.equals("pointproche")){
					plopR.setProche(OsmDataConvertor.OsmToPoint(reader.nextString()));
				}else if(key.equals("pointfourni")){
					plopR.setFourni(OsmDataConvertor.OsmToPoint(reader.nextString()));
				}else if(key.equals("distancefromroad")){
					plopR.setDistance(Float.valueOf(String.valueOf(reader.nextDouble())));
				}else{
					reader.skipValue();
				}
			
		} catch (Exception e) {
		}
		return plopR;
	}

}
