package fr.project.campusroad.api.parser;

import java.io.IOException;

import android.util.JsonReader;
import android.util.Log;

public abstract class JSONParser {
	JsonReader reader = null;
	public JSONParser(JsonReader json){
		reader = json;
	}
	
	public Object runParsing() throws Exception{
		throw new Exception("Not implemented");
	}
	
	protected Object runParsingArray() throws Exception{
		return parseArray();
	}
	
	private Object parseArray() throws Exception{
		reader.beginArray();
		while(reader.hasNext()){
			parseNextObject();
		}
		reader.endArray();
		return null;
	}
	
	protected Object runParsingObject() throws Exception{
		return parseNextObject();
	}
	
	protected Object parseNextObject() throws Exception {
		throw new Exception("Not implemented");
	}

	protected Object storeValue(String key, Object plop) {
		return plop;
		// TODO Auto-generated method stub
		
	}
	
}
