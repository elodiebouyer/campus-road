package fr.project.campusroad.api.parser;

import fr.project.campusroad.data.MinMax;
import fr.project.campusroad.data.Road;
import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

public class ParserMinMax extends JSONParser{

	public ParserMinMax(JsonReader json) {
		super(json);
	}
	
	@Override
	public Object runParsing() throws Exception{
		try{
			runParsingObject();
		}catch(Exception e){
		}
		if(MinMax.getXmax() == null)
			return false;
		return true;
	}
	
	@Override
	protected Object parseNextObject() throws Exception {
		reader.beginObject();
		while(reader.hasNext()){
			storeValue(reader.nextName(), null);
		}
		reader.endObject();
		return null;
	}

	@Override
	protected Object storeValue(String key, Object plop){
		try {
			if (reader.peek() == JsonToken.NULL) {
				reader.nextNull();
	            return null;
	          }
				if(key.equals("xmin")){
					MinMax.setXmin(Float.valueOf(Double.valueOf(reader.nextDouble()).toString()));
				}else if(key.equals("ymin")){
					MinMax.setYmin(Float.valueOf(Double.valueOf(reader.nextDouble()).toString()));
				}else if(key.equals("ymax")){
					MinMax.setYmax(Float.valueOf(Double.valueOf(reader.nextDouble()).toString()));
				}else if(key.equals("xmax")){
					MinMax.setXmax(Float.valueOf(Double.valueOf(reader.nextDouble()).toString()));
				}else{
					reader.skipValue();
				}
			
		} catch (Exception e) {
			Log.e("MinMaxErr", e.getMessage());
		}
		return null;
	}
}
