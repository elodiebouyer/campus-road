package fr.project.campusroad.api.parser;

import fr.project.campusroad.data.Road;
import fr.project.campusroad.data.Roads;
import android.util.JsonReader;
import android.util.JsonToken;

public class ParserRoads extends JSONParser {
	
	private Roads roads;

	public ParserRoads(JsonReader json) {
		super(json);
		roads = new Roads();
	}
	
	@Override
	protected Object parseNextObject() throws Exception {
		Road r = new Road();
		reader.beginObject();
		while(reader.hasNext()){
			r = (Road) storeValue(reader.nextName(), r);
		}
		reader.endObject();
		roads.addRoad(r);
		return r;
	}
	
	@Override
	public Object runParsing() throws Exception{
		runParsingArray();
		return roads;
	}
	
	@Override
	protected Object storeValue(String key, Object plop){
		Road plopR = (Road) plop;
		
		try {
			if (reader.peek() == JsonToken.NULL) {
				reader.nextNull();
	            return plopR;
	          }
				if(key.equals("osm_id")){
					plopR.setId(reader.nextInt());
				}else if(key.equals("name")){
					plopR.setName(reader.nextString());
				}else if(key.equals("textway")){
					plopR.setRoadParts(reader.nextString());
				}else{
					reader.skipValue();
				}
			
		} catch (Exception e) {
		}
		return plopR;
	}

}
