package fr.project.campusroad.api.parser;

import java.util.ArrayList;
import java.util.List;

import fr.project.campusroad.Geometry.Point;
import fr.project.campusroad.Geometry.Segment;

public class OsmDataConvertor {

	public static List<Segment> OsmToSegments(String osmData){
		return null;
	}
	
	public static Point OsmToPoint(String osmData){
		int start = osmData.indexOf('(');
		int end = osmData.indexOf(')');
		String [] tmp = osmData.substring(start+1, end-1).split(" ");
		return new Point(Float.valueOf(tmp[0]), Float.valueOf(tmp[1]));
	}
	
	public static List<Point> OsmToPoints(String osmData){
		List<Point> points = new ArrayList<Point>();
		int start = osmData.indexOf('(');
		int end = osmData.indexOf(')');
		String [] tmp;
		for(String s:osmData.substring(start+1, end-1).split(",")){
			tmp = s.split(" ");
			points.add(new Point(Float.parseFloat(tmp[0]), Float.parseFloat(tmp[1])));
		}
		return points;
	}
}
