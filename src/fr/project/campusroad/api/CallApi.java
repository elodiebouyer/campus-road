package fr.project.campusroad.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import fr.project.campusroad.api.parser.*;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.JsonReader;
import android.util.Log;

public class CallApi extends Thread {
	private final static String API_URL = "http://pro-timing.com:8080/";
	public final static String API_INSERT = "PUT";
	public final static String API_GET = "GET";
	public final static String API_POST = "POST";
	
	private String url = null;
	private String methode = null;
	private String obj = null;
	private Handler handler = null;
	private ApiType type = null;
	
    public CallApi(Handler handler,String operation,ApiType type, String ID, String toSend) throws Exception {
    	this.type = type;
    	switch(type){
    		case API_BUILDINGS_TYPE:
    			url = API_URL + "buildings";
    			break;
    		case API_ROADS_TYPE:
    			url = API_URL + "roads";
    			break;
    		case API_MIN_MAX_TYPE:
    			url = API_URL + "";
    			break;
    		case API_PLACEMENT:
    			url = API_URL + "placement";
    			break;
    		default:
    			throw new Exception("Type inconnu.");
    	}
    	
		if(ID != null){
			url += "/" + ID;
		}
		if(toSend != null){
			throw new UnsupportedOperationException("Pas implementé.");
		}
		
		methode = operation;
		obj = toSend;
		this.handler = handler;
	}
    
    @Override
    public void run(){
    	try {
    		RetrieveAPI api = new RetrieveAPI();
    		api.execute(url);
    	} catch (Exception e ) {
            Log.e("CallApi", e.toString());
    	}
    	
    }
    
    class RetrieveAPI extends AsyncTask<String, Void, Object> {

        protected Object doInBackground(String... urls) {
            try {
            	Object res = null;
            	URL _url = new URL(url);
        		URLConnection urlConnection = _url.openConnection();
        		BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        		JsonReader json = new JsonReader(reader);
        		JSONParser parser = null;
        		switch(type){
					case API_ROADS_TYPE:
						parser = new ParserRoads(json);
	        			res = parser.runParsing();
						break;
					case API_PLACEMENT:
						parser = new ParserPlacement(json);
	        			res = parser.runParsing();
						break;
					case API_MIN_MAX_TYPE:
						parser = new ParserMinMax(json);
	        			res = parser.runParsing();
						break;
					default:
				}
        		
                
        		return res;
            } catch (Exception e) {
                return null;
            }
        }

        protected void onPostExecute(Object content) {
        	Message msg = Message.obtain();
            msg.what = type.ordinal();
            msg.obj = content;
            if(handler != null)
            	handler.sendMessage(msg);
        }
    }
} 