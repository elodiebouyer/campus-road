package fr.project.campusroad.dataBase;

import java.util.Collections;
import java.util.List;

import fr.project.campusroad.Geometry.Point;
import fr.project.campusroad.data.Roads;

/**
 * Created by centrois on 11/12/14.
 */
public class ExchangeBDD {

    	public Roads getAllBasicScore(RoadBDD _pointBdd) {
		_pointBdd.open();
		Roads roads = _pointBdd.getAllRoads();
		_pointBdd.close();
		return roads;

	}

}
