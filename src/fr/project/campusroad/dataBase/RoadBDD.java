package fr.project.campusroad.dataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import fr.project.campusroad.Geometry.Point;
import fr.project.campusroad.data.Road;
import fr.project.campusroad.data.Roads;

/**
 * Created by centrois on 11/12/14.
 */
public class RoadBDD {

    private static final String TAG = "RoadBDD";

    private static final int VERSION_BDD = 2;
    private static final String NAME_BDD = "dataCampus.db";

    private static final String TAB_ROAD = "Roads";

    private static final String COL_ID = "ID";
    private static final int NUM_COL_ID = 0;

    private static final String COL_ID_ROAD = "ID_ROAD";
    private static final int NUM_COL_ID_ROAD = 1;

    private static final String COL_COORD = "COORD";
    private static final int NUM_COL_COORD = 2;

    private String[] allColumns = { COL_ID, COL_ID_ROAD,COL_COORD};
	private SQLiteDatabase bdd;

	private DataBase myBaseSQLite;

    public RoadBDD(Context context) {
        // BDD Creation
        this.myBaseSQLite = new DataBase(context, NAME_BDD, null, VERSION_BDD);
    }

    public void open() {
        // Opening BDD write
        bdd = myBaseSQLite.getWritableDatabase();
    }

    public void close() {
        // Closing access to BDD
        bdd.close();
    }

    public SQLiteDatabase getBDD() {
        return bdd;
    }

    public DataBase getDataBase(){
        return this.myBaseSQLite;
    }

    public long insertPoint(int idRoad, Road tmp_road) {
        ContentValues values = new ContentValues();

        String roadSerialise = "";
        for (Point tmp_point : tmp_road.getRoadParts()) {
            roadSerialise += tmp_point.toString() + ";";
        }
        values.put(COL_ID_ROAD, idRoad);
        values.put(COL_COORD, roadSerialise);
        return bdd.insert(TAB_ROAD, null, values);
    }

    public int removePointWithID(int id) {
        return bdd.delete(TAB_ROAD, COL_ID + " = " + id, null);
    }


    public List<Point> getAllRoadSPoint(int idRoad) {

		List<Point> allPoint = new ArrayList<Point>();
		
		Cursor cursor = bdd.query(TAB_ROAD, allColumns, "ID_ROAD = ?", new String[] {Integer.toString(idRoad)}, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
            String p = cursorToPoint(cursor);

            String[] pointSlip = p.split(";");
            for(String coordonnes: pointSlip){
                String[] coordonne = coordonnes.split(" ");

                allPoint.add(new Point(Float.parseFloat(coordonne[0]), Float.parseFloat(coordonne[1])));
            }
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return allPoint;
	}

    public Roads getAllRoads() {

        Roads roads = new Roads();
        Cursor cursor = bdd.query(TAB_ROAD, allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Road road = new Road();
            String p = cursor.getString(NUM_COL_COORD);
            for (String coordonnes: p.split(";")) {
                String []coordonne = coordonnes.split(" ");
                road.getRoadParts().add(new Point(Float.parseFloat(coordonne[0]), Float.parseFloat(coordonne[1])));
            }

            roads.addRoad(road);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return roads;
    }

    private String cursorToPoint(Cursor cursor) {
        String point = cursor.getString(NUM_COL_COORD);
        return point;
    }
}
