package fr.project.campusroad.dataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import fr.project.campusroad.Geometry.Point;
import fr.project.campusroad.calculs.RegisterPath;
import fr.project.campusroad.calculs.UserPath;
import fr.project.campusroad.data.Path;
import fr.project.campusroad.data.Road;
import fr.project.campusroad.data.Roads;

/**
 * Created by centrois on 22/12/14.
 */
public class PathBDD {

    private static final String TAG = "PathBDD";

    private static final int VERSION_BDD = 2;
    private static final String NAME_BDD = "dataCampus.db";

    private static final String TAB_PATH = "Paths";

    private static final String COL_ID = "ID";
    private static final int NUM_COL_ID = 0;

    private static final String COL_NAME = "NAME";
    private static final int NUM_COL_NAME = 1;

    private static final String COL_COORD = "COORD";
    private static final int NUM_COL_COORD = 2;

    private String[] allColumns = { COL_ID,COL_NAME,COL_COORD};
    private SQLiteDatabase bdd;

    private DataBase myBaseSQLite;

    public PathBDD(Context context) {
        // BDD Creation
        this.myBaseSQLite = new DataBase(context, NAME_BDD, null, VERSION_BDD);
    }

    public void open() {
        // Opening BDD write
        bdd = myBaseSQLite.getWritableDatabase();
    }

    public void close() {
        // Closing access to BDD
        bdd.close();
    }

    public SQLiteDatabase getBDD() {
        return bdd;
    }

    public DataBase getDataBase(){
        return this.myBaseSQLite;
    }

    public long insertRoad(UserPath up) {
        ContentValues values = new ContentValues();

        String roadSerialise = "";
        for (Point tmp_point : up.getPath().getPoint()) {
            roadSerialise += tmp_point.toString() + ";";
        }
        Log.i("test", up.getName() + ":" + roadSerialise);
        values.put(COL_NAME, up.getName());
        values.put(COL_COORD, roadSerialise);
        return bdd.insert(TAB_PATH, null, values);
    }

    public int removePointWithID(int id) {
        return bdd.delete(TAB_PATH, COL_ID + " = " + id, null);
    }

    public List<UserPath> getAllPath() {

        List<UserPath> ups = new ArrayList<UserPath>();
        Cursor cursor = bdd.query(TAB_PATH, allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Path p = new Path();
            String allCoords = cursor.getString(NUM_COL_COORD);
            if (!allCoords.equals("")) {
                for (String coords : allCoords.split(";")) {
                    String[] coord = coords.split(" ");
                    p.addPoint(new Point(Float.parseFloat(coord[0]), Float.parseFloat(coord[1])));
                }
            }
            UserPath up = new UserPath(cursor.getString(NUM_COL_NAME),p);
            ups.add(up);

            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return ups;
    }


}
