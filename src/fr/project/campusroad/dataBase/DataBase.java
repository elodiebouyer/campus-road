package fr.project.campusroad.dataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBase extends SQLiteOpenHelper {


    // Table TRecapPlayer and its field
    private static final String TAB_ROAD = "Roads";
    private static final String COL_ID = "ID";
    private static final String COL_ID_ROAD = "ID_ROAD";
    private static final String COL_COORD= "COORD";

    private static final String TAB_PATH = "Paths";
    private static final String COL_NAME = "NAME";
    // Complete request for create Road
    private static final String CREATE_TABLE_ROAD = "CREATE TABLE "
            + TAB_ROAD + " (" + COL_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COL_ID_ROAD + " INTEGER, " +
            COL_COORD + " STRING);";

    // Complete request for create Road
    private static final String CREATE_TABLE_PATH = "CREATE TABLE "
            + TAB_PATH + " (" + COL_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COL_NAME + " STRING, " +
            COL_COORD + " STRING);";


    public DataBase(Context context, String name, CursorFactory factory,
                    int version) {
        super(context, name, factory, version);
    }

    /**
     * Cette méthode est appelée lors de la toute première création de la base
     * de données. Ici, on doit créer les tables et éventuellement les populer.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_ROAD);
        db.execSQL(CREATE_TABLE_PATH);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on supprime la table table_contacts de la BDD et on recrée la BDD
        db.execSQL("DROP TABLE " + TAB_ROAD + ";");
        db.execSQL("DROP TABLE " + TAB_PATH + ";");
        onCreate(db);
    }
    public void delete(SQLiteDatabase db) {
        // on supprime la table table_contacts de la BDD et on recrée la BDD
        db.execSQL("DROP TABLE " + TAB_ROAD + ";");
        db.execSQL("DROP TABLE " + TAB_PATH + ";");
        onCreate(db);
    }



}
