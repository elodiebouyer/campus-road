package fr.project.campusroad.Geometry;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.opengles.GL10;


public class Rectangle {
	
	private FloatBuffer sommets;
	private int nbSommets;
	
	private ShortBuffer indices;
	private int nbIndices;
	
	private float tab[] = { 
			-1,     -1,     0, 
			-1,     -0.25f, 0,   
			-0.75f, -0.25f, 0, 
			-0.75f, -1,     0, 
	};
	
	private short ind[] = {0,1,  2,0, 3,2};
	
	
	public Rectangle() {
		super();
		nbSommets = tab.length/3;
		nbIndices = ind.length;
		
		init();
	}
	
	public void init() {
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(3 * Float.SIZE * nbSommets);
		byteBuffer.order(ByteOrder.nativeOrder());

		this.sommets = byteBuffer.asFloatBuffer();	
		this.sommets.put(tab);
		this.sommets.flip();

		ByteBuffer byteBufferI = ByteBuffer.allocateDirect(this.nbIndices* Short.SIZE);
		byteBufferI.order(ByteOrder.nativeOrder());

		this.indices = byteBufferI.asShortBuffer();
		this.indices.put(ind);
		this.indices.flip(); 
	}
	
	public void draw(GL10 gl) {

		gl.glPushMatrix();
		gl.glTexCoordPointer(nbSommets, GL10.GL_FLOAT, 0, sommets);

		gl.glTranslatef(0, 0, 0);
		gl.glColor4f(0, 0, 0.7f, 1);
		
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, this.sommets);
		gl.glDrawElements(GL10.GL_TRIANGLES, nbIndices, GL10.GL_UNSIGNED_SHORT, indices);

		gl.glPopMatrix();
	}

}
