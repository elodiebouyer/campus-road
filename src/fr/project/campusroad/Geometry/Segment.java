package fr.project.campusroad.Geometry;

public class Segment {
	
	private Point beg;
	private Point end;
	
	Segment(Point beg, Point end){
		this.setBeg(beg);
		this.setEnd(end);
		
	}
	/*
	 * Add vector v to the current one.
	 */
	public void addVector(Segment v){
		this.end = v.getEnd();
	}
	
	/*
	 * calcul the vector size 
	 */
	public float size(){
		float x = (this.getEnd().getX() - this.getBeg().getX())*2;
		float y = (this.getEnd().getY() - this.getBeg().getY())*2;
		
		return (float) Math.sqrt(x+y);
	}

	/*
	 * Calcul the difference on X's axe between 2 vector's Point
	 * (height's vector) 
	 */
	public float diffX(){
		return this.getEnd().getX() - this.getBeg().getX();
	}
	
	/*
	 * Calcul the difference on Y's axe between 2 vector's Point
	 * (length's vector) 
	 */
	public float diffY(){
		return this.getEnd().getY() - this.getBeg().getY();
	}
	
	public void coef(float c ){
		
		this.getEnd().setX(((this.getEnd().getX() - this.getBeg().getX())*c) + this.getBeg().getX());
		this.getEnd().setY(((this.getEnd().getY() - this.getBeg().getY())*c) + this.getBeg().getY());
	}
	
	public Point getBeg() {
		return beg;
	}

	public void setBeg(Point beg) {
		this.beg = beg;
	}

	public Point getEnd() {
		return end;
	}

	public void setEnd(Point end) {
		this.end = end;
	}

}