package fr.project.campusroad.Geometry;

public class Point {
	
	private float x;
	private float y;

    public Point(){
        this.setX(0);
        this.setY(0);
    }

	public Point(float f, float g){
		this.setX(f);
		this.setY(g);
	}


	public void addVector(Segment v){
		
		this.setX(this.getX() + v.diffX());
		this.setY(this.getX() + v.diffY());
	}

    public String toString(){
        String pointToString = this.getX() + " " + this.getY();
        return pointToString;
    }
	
	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

}