package fr.project.campusroad.Geometry;

public class Operation {
	
	/*
	 * Add vector v1 vector v2.
	 * Return the new vector v.
	 */
	public Segment addVector(Segment v1, Segment v2){	
		return new Segment(v1.getBeg(), v2.getEnd());
	}
	
	/*
	 * Add point p1 to point p2.
	 * Return the new Point p.
	 */
	public Point addPoint(Point p1, Point p2){	
		return new Point(p1.getX() + p2.getX(), p1.getY() + p2.getY());
	}
	
	
	public float distancePointDroite(Point p, Segment v){
		
		Segment u = new Segment(v.getBeg(), p);
		Segment temp = this.addVector(u, v);
		temp.coef(1/u.size());
		
		return temp.size();
	}
	
	public Point projection(Segment v, Point p, Segment direction){
		
		direction.setEnd(new Point(direction.getEnd().getX()-(direction.getBeg().getX()-p.getX()), direction.getEnd().getY()-(direction.getBeg().getY()-p.getY()) ));
		direction.setBeg(p);
		
		
		Segment tempUP = this.addVector(new Segment(p, v.getEnd()), v);
		Segment tempDown = this.addVector(direction, v);
		
		return null;
	}
	
}