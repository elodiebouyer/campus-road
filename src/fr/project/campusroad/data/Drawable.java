package fr.project.campusroad.data;

import javax.microedition.khronos.opengles.GL10;

public interface Drawable {
	public void draw(GL10 gl, float zoom);
}
