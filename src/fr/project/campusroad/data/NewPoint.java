package fr.project.campusroad.data;

import fr.project.campusroad.Geometry.Point;

public interface NewPoint {
	public Point getPoint();
}
