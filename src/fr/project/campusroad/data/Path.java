package fr.project.campusroad.data;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import android.util.Log;
import fr.project.campusroad.Geometry.Point;

public class Path implements Drawable {
	private List<Point> path = null;
	private FloatBuffer sommets;
	private ShortBuffer indices;
	private float lastZoom = 1;
	
	public Path(){
		this.path = new ArrayList<Point>();
	}
	
	public void addPoint(Point p){
		path.add(p);
		listToOpenGL(lastZoom);
	}
	
	public void clean(){
		path.clear();
	}
	
	public List<Point> getPoint() {
		return path;
	}
	
	private void listToOpenGL(float z){
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(3 * Float.SIZE * path.size());
		byteBuffer.order(ByteOrder.nativeOrder());

		float newx, newy = 0F;
		float newmax = MinMax.getXmax()-MinMax.getXmin();
		float newminx = MinMax.getXmin();
		float newmaxy = MinMax.getYmax()-MinMax.getYmin();
		float newminy = MinMax.getYmin();

		if( sommets != null ) this.sommets.clear();
		this.sommets = byteBuffer.asFloatBuffer();
		for(Point p:path){			
			newx = (p.getX()-newminx)/newmax;
			newy = (p.getY()-newminy)/newmaxy;
			this.sommets.put((newx*2-1)*z);
			this.sommets.put((newy*2-1)*z);
			this.sommets.put(0);
		}
		this.sommets.flip();

		ByteBuffer byteBufferI = ByteBuffer.allocateDirect((path.size())* Short.SIZE);
		byteBufferI.order(ByteOrder.nativeOrder());

		this.indices = byteBufferI.asShortBuffer();
		for(short i = 0; i < path.size(); i++){
			this.indices.put(i);
		}
		this.indices.flip(); 
	}

	@Override
	public void draw(GL10 gl, float zoom) {
		if( lastZoom != zoom ) {
			listToOpenGL(zoom);
			lastZoom = zoom;
		}
		if(sommets == null) return;
		gl.glLineWidth(5f);
		gl.glPushMatrix();
		gl.glTexCoordPointer( path.size(), GL10.GL_FLOAT, 0, sommets);

		gl.glTranslatef(0, 0, 0);
		gl.glColor4f(0f, 0f, 1f, 0.7F);

		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, this.sommets);
		gl.glDrawElements(GL10.GL_LINE_STRIP, path.size(), GL10.GL_UNSIGNED_SHORT, indices);
		

		gl.glPopMatrix();
		gl.glLineWidth(1f);
		
	}
	
	public Integer size(){
		return path.size();
	}
}
