package fr.project.campusroad.data;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.LinkedList;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import fr.project.campusroad.Geometry.Point;
import fr.project.campusroad.api.parser.OsmDataConvertor;

public class Road implements Drawable{
	private Integer id;
	private String name;
	private List<Point> roadParts = null;
	private FloatBuffer sommets;
	private ShortBuffer indices;
	private int nbIndices;
	private float lastZoom = 2;

	public Road(){
		roadParts = new LinkedList<Point>();
	}

	public Road(Integer osm_id, String name, String road){
		roadParts = OsmDataConvertor.OsmToPoints(road);
		this.name = name;
		this.id = osm_id;

		listToOpenGL(lastZoom);
	}

    public Road(Integer id, String name, List<Point> p){
        this.roadParts = p;
        this.name = name;
        this.id = id;

        listToOpenGL(lastZoom);
    }

	private void listToOpenGL(float z){
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(3 * Float.SIZE * roadParts.size());
		byteBuffer.order(ByteOrder.nativeOrder());
		float newx, newy = 0F;
		float newmax = MinMax.getXmax()-MinMax.getXmin();
		float newminx = MinMax.getXmin();
		float newmaxy = MinMax.getYmax()-MinMax.getYmin();
		float newminy = MinMax.getYmin();

		if( sommets != null ) this.sommets.clear();
		this.sommets = byteBuffer.asFloatBuffer();
		for(Point p:roadParts){			
			newx = (p.getX()-newminx)/newmax;
			newy = (p.getY()-newminy)/newmaxy;
			this.sommets.put((newx*2-1)*z);
			this.sommets.put((newy*2-1)*z);
			this.sommets.put(0);
		}
		this.sommets.flip();

		ByteBuffer byteBufferI = ByteBuffer.allocateDirect((roadParts.size())* Short.SIZE);
		byteBufferI.order(ByteOrder.nativeOrder());

		this.indices = byteBufferI.asShortBuffer();
		for(short i = 0; i < roadParts.size(); i++){
			this.indices.put(i);
		}
		this.indices.flip(); 
		nbIndices = roadParts.size();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Point> getRoadParts() {
		return roadParts;
	}

	public void setRoadParts(List<Point> roadParts) {
		this.roadParts = roadParts;
	}

	public void setRoadParts(String osm_data) {
		this.roadParts = OsmDataConvertor.OsmToPoints(osm_data);
		listToOpenGL(lastZoom);
	}
	
	public void draw(GL10 gl, float zoom) {
		if( lastZoom != zoom ) {
			listToOpenGL(zoom);
			lastZoom = zoom;
		}
		if(sommets == null) return;
		gl.glLineWidth(2f);
		gl.glPushMatrix();
		
		gl.glTexCoordPointer( roadParts.size(), GL10.GL_FLOAT, 0, sommets);

		gl.glColor4f(0f, 0f, 0f, 1);

		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, this.sommets);
		gl.glDrawElements(GL10.GL_LINE_STRIP, nbIndices, GL10.GL_UNSIGNED_SHORT, indices);
		
		gl.glPopMatrix();
		gl.glLineWidth(1f);
	}

}
