package fr.project.campusroad.data;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.opengles.GL10;

public class Position implements Drawable{
	private Float x = null;
	private Float y = null;
	private Float z = null;
	private FloatBuffer sommets = null;
	private ShortBuffer indices = null;
	private float lastZoom = 2;

	public Position(){
		x = y = 0F;
		z = 1F;
	}

	public Position(Float x, Float y, Float z){
		this.x=x;
		this.y=y;
		this.z=1F;

		listToOpenGL(lastZoom);
	}

	public void setX(Float x) {
		this.x = x;
		listToOpenGL(lastZoom);
	}
	public Float getX() {
		return this.x;
	}
	public void setY(Float y) {
		this.y = y;
		listToOpenGL(lastZoom);
	}
	public Float getY() {
		return this.y;
	}
	private void listToOpenGL(float zoom){
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(3 * Float.SIZE * 1);
		byteBuffer.order(ByteOrder.nativeOrder());

		float newx, newy = 0F;
		if(MinMax.getXmax()==null)return;
		float newmax = MinMax.getXmax()-MinMax.getXmin();
		float newminx = MinMax.getXmin();
		float newmaxy = MinMax.getYmax()-MinMax.getYmin();
		float newminy = MinMax.getYmin();

		this.sommets = byteBuffer.asFloatBuffer();

		newx = (x-newminx)/newmax;
		newy = (y-newminy)/newmaxy;

		this.sommets.put((newx*2-1)*zoom);
		this.sommets.put((newy*2-1)*zoom);
		this.sommets.put(-1); 
		this.sommets.flip();

		ByteBuffer byteBufferI = ByteBuffer.allocateDirect(1* Short.SIZE);
		byteBufferI.order(ByteOrder.nativeOrder());

		this.indices = byteBufferI.asShortBuffer();
		this.indices.put((short) 0);
		this.indices.flip(); 
	}

	@Override
	public void draw(GL10 gl, float zoom) {
		if( lastZoom != zoom ) {
			listToOpenGL(zoom);
			lastZoom = zoom;
		}
		if(this.sommets == null) return;

		gl.glPushMatrix();
		gl.glPointSize(5.0f);

		gl.glTexCoordPointer( 1, GL10.GL_FLOAT, 0, sommets);

		gl.glColor4f(1f, 0f, 0f, 1);

		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, this.sommets);
		gl.glDrawElements(GL10.GL_POINTS, 1, GL10.GL_UNSIGNED_SHORT, indices);

		gl.glPopMatrix();
	}

}
