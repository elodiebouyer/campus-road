package fr.project.campusroad.data;

import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import android.util.Log;

public class Roads  implements Drawable{


    private List<Road> roads;

    public List<Road> getRoads() { return roads; }
	public Roads(){
		roads = new ArrayList<Road>();
	}
	
	public void addRoad(Road r){
		roads.add(r);
	}
	
	public void draw(GL10 gl, float zoom){
		for(Road r:roads){
			r.draw(gl, zoom);
		}
	}
}
