package fr.project.campusroad.data;

import fr.project.campusroad.Geometry.Point;

public class PointPlacement implements NewPoint {
	private Point proche = null;
	private Point fourni = null;
	private Float distance = null;
	
	public PointPlacement(){
		
	}

	public Point getProche() {
		return proche;
	}

	public void setProche(Point proche) {
		this.proche = proche;
	}
	
	public Point getFourni() {
		return fourni;
	}

	public void setFourni(Point fourni) {
		this.fourni = fourni;
	}

	public Float getDistance() {
		return distance;
	}

	public void setDistance(Float distance) {
		this.distance = distance;
	}
	
	@Override
	public Point getPoint(){
		if(distance < 5F)
			return proche;
		return fourni;
	}

}
