package fr.project.campusroad.data;

public class MinMax {
	private static Float xmin;
	private static Float xmax;
	private static Float ymin;
	private static Float ymax;
	
	public static Float getXmin() {
		return xmin;
	}
	public static void setXmin(Float xmin) {
		MinMax.xmin = xmin;
	}
	public static Float getXmax() {
		return xmax;
	}
	public static void setXmax(Float xmax) {
		MinMax.xmax = xmax;
	}
	public static Float getYmin() {
		return ymin;
	}
	public static void setYmin(Float ymin) {
		MinMax.ymin = ymin;
	}
	public static Float getYmax() {
		return ymax;
	}
	public static void setYmax(Float ymax) {
		MinMax.ymax = ymax;
	}
	
}
