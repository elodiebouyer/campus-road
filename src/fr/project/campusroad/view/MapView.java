package fr.project.campusroad.view;


import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;


public class MapView extends GLSurfaceView {

	private MapRenderer mRenderer;

	public MapView(Context context) {
		super(context);
		mRenderer = new MapRenderer(getWidth(), getHeight());
		setRenderer(mRenderer);
	}

	public void addDrawable(Object o){
		mRenderer.addDrawable(o);
	}

	public void move(Direction direction, float mvt) {
		switch(direction) {
		case Left:
			mRenderer.left(mvt);
			break;
			
		case Right:
			mRenderer.right(mvt);
			break;

		case Bottom:
			mRenderer.bottom(mvt);
			break;
			
		case Top:
			mRenderer.top(mvt);
			break;
			
		case Minus:
			mRenderer.minus(mvt);
			break;
			
		case Plus:
			mRenderer.plus(mvt);
			break;

		default:
			break;
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent e) {
		return true;
	}

}
