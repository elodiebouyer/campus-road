package fr.project.campusroad.view;

public enum Direction {
	Left,
	Right,
	Bottom,
	Top,
	Plus,
	Minus
}
