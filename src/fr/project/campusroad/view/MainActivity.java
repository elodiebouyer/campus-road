package fr.project.campusroad.view;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import fr.project.campusroad.R;
import fr.project.campusroad.api.ApiType;
import fr.project.campusroad.api.CallApi;
import fr.project.campusroad.calculs.RegisterPath;
import fr.project.campusroad.calculs.UserPath;
import fr.project.campusroad.calculs.online.RegisterPathOnline;
import fr.project.campusroad.data.Position;
import fr.project.campusroad.data.Road;
import fr.project.campusroad.data.Roads;
import fr.project.campusroad.dataBase.PathBDD;
import fr.project.campusroad.dataBase.RoadBDD;
import fr.project.campusroad.gps.Gps;


public class MainActivity extends Activity {

	private GLSurfaceView mGlSurfaceView;
	private Position position = null;
	private String mode = "online"; // ou "offline" pour plus tard
	private String stateRegister = "off"; // ou "on"
	private RegisterPath registerPath = null;
    private List<UserPath> userPaths = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		callApi();
		setLayout();
		setGps();

		if(mode.equals("online")){
			registerPath = new RegisterPathOnline();
			((MapView) mGlSurfaceView).addDrawable(registerPath.getPath());
		}
	}


	private void callApi() {
		Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case 2:
					if((Boolean)msg.obj == true){
						Log.d("MAP", "Reception MAP lancement appli");
						synchonizeMapData();
					}
					else
						Toast.makeText(getApplicationContext(), "Problème de connexion au serveur", Toast.LENGTH_LONG).show();
                    break;
				default:
					break;
				}
			}
        };
		CallApi api;
		try {
			api = new CallApi(handler,CallApi.API_GET, ApiType.API_MIN_MAX_TYPE, null, null);
			api.run();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setLayout() {
		setContentView(R.layout.activity_main);

		mGlSurfaceView = new MapView(this);
		LinearLayout map = (LinearLayout) findViewById(R.id.mapid);
		map.addView(mGlSurfaceView);
	}

	private void setGps() {
		new Gps(this);
		position = new Position(0F, 0F, 0F);
		((MapView)mGlSurfaceView).addDrawable(position);
	}

	@Override
	protected void onPause() {
		super.onPause();
		mGlSurfaceView.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		mGlSurfaceView.onResume();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if( id == R.id.action_save ) {
			if( stateRegister.equals("on") ){
				savePath();
			} 
			else {
				stateRegister = "on";
				registerPath.cancel(); // au cas ou on repart sur une bonne base
				Toast.makeText(getApplicationContext(), "Début d'enregistrement du trajet.", Toast.LENGTH_LONG).show();
			}
			return true;
		}
		else if( id == R.id.action_see_other) {
			displayOldPath();
			return true;
		}
		else if( id == R.id.action_delete) {
			if(this.stateRegister.equals("on")){
				Toast.makeText(getApplicationContext(), "Suppression du trajet.", Toast.LENGTH_LONG).show();
				registerPath.cancel();
				this.stateRegister = "off";
			}
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * Open a box dialog to choice a path name.
	 */
	private void savePath() {
		AlertDialog.Builder box = new AlertDialog.Builder(this);
		final EditText input = new EditText(this);

		box.setView(input);
		box.setTitle(getString(R.string.box_save_title));
		box.setMessage(getString(R.string.box_save_message));
		box.setPositiveButton(getString(R.string.box_save_ok_button), new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				String name;

				if( TextUtils.isEmpty(input.getText().toString())) {
					name = getString(R.string.unknown);
				} else {
					name = input.getText().toString();
					registerPath.save(name);
                    stateRegister = "off";
				}
                if(registerPath.getPath().size() != 0) {
                    final PathBDD pathBdd = new PathBDD(getApplicationContext());
                    pathBdd.open();
                    pathBdd.insertRoad(new UserPath(name, registerPath.getPath()));

                    Toast.makeText(getApplicationContext(), "Trajet " + name + " sauvegardé", Toast.LENGTH_SHORT).show();
                }
                Toast.makeText(getApplicationContext(), "Trajet " + name + " non sauvegardé : Vous n'avez pas bougé !", Toast.LENGTH_SHORT).show();
			}
		});

        	box.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			}
		}
				);
		box.show();
	}

	/**
	 * Display all old path.
	 */
	private void displayOldPath() {
        try {
            final PathBDD pathBdd = new PathBDD(this);
            pathBdd.open();
            userPaths = pathBdd.getAllPath();
        }
        catch (Exception e){
            Toast.makeText(getApplicationContext(), "Base déjà en cours d'utilisation !", Toast.LENGTH_LONG).show();
        }

        if (!userPaths.isEmpty()){
            Toast.makeText(getApplicationContext(), "Chargement de vos trajets enregistrés", Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(getApplicationContext(), "Aucun trajet enregistré", Toast.LENGTH_LONG).show();
        }
		AlertDialog.Builder box = new AlertDialog.Builder(this);
        registerPath.setmUserPaths(userPaths);
		box.setTitle("Mes trajets");
		box.setItems(registerPath.getNames(), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				registerPath.setCurrentPath(which);
			}
		});
		box.show();
	}


	public void synchonizeMapData() {

		final RoadBDD pointBdd = new RoadBDD(this);
		pointBdd.open();
		final Roads allRoads = pointBdd.getAllRoads();

		if (allRoads.getRoads().size() != 0) {
			((MapView) mGlSurfaceView).addDrawable(allRoads);

		}
		else {
			Handler handler = new Handler() {
				@Override
				public void handleMessage(Message msg) {
					Object res = null;
					switch (msg.what) {
					case 0:
						Log.d("Main", "Réception de building");
						res = msg.obj;
						break;
					case 1:
						Log.d("Main", "Réception de route");
						res = msg.obj;
						((MapView) mGlSurfaceView).addDrawable(res);
                        Log.i("Main", "Debut ajout points en base");
                        if (res != null) {
                            Toast.makeText(getApplicationContext(), "Points en cours d'ajout, patientez...", Toast.LENGTH_LONG).show();

                            final Object finalRes = res;
                            new Thread(new Runnable() {

                                public void run() {
                                    addRoadsToDataBase((Roads) finalRes, pointBdd);
                                    }
                            }).start();

                        }

                        break;
					default:
						break;
					}
					if (res == null) {
						Toast.makeText(getApplicationContext(), "Problème dans la reception des données", Toast.LENGTH_LONG).show();
					} else {
						Toast.makeText(getApplicationContext(), "Pas encore implémenté !", Toast.LENGTH_LONG).show();
					}
				}


                public void addRoadsToDataBase(Roads res, RoadBDD pointBdd) {

                    List<Road> tmp_allRoads = res.getRoads();
                    Log.i("Main", "Ajout des routes a la Bdd :" + tmp_allRoads.size());
                    int idRoad = 1;

                    for(Road tmp_road : tmp_allRoads) {
                        pointBdd.insertPoint(idRoad, tmp_road);
                        idRoad++;
                    }
                    Log.i("Main", "Route ajoutée");
                   }



            };
			CallApi api;
			try {
				api = new CallApi(handler, CallApi.API_GET, ApiType.API_ROADS_TYPE, null, null);
				api.run();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
        //pointBdd.close();
	}


	/**
	 * When the location changed.
	 * Receive GPS data.
	 */
	public void onLocationChanged(double longitude, double latitude) {
		position.setX((float)longitude);
		position.setY((float) latitude);

		if( stateRegister.equals("on") ){
			try {
				registerPath.addPoint(new Position(position.getX(), position.getY(), 0F));
			} catch (Exception e) {}
		}
	}

	private final float mvt = 0.10f;
	private final float zoom = 0.5f;

	public void moveLeft(View v) {
		((MapView) mGlSurfaceView).move(Direction.Left, mvt);
	}

	public void moveRight(View v) {
		((MapView) mGlSurfaceView).move(Direction.Right, mvt);
	}

	public void moveBottom(View v) {
		((MapView) mGlSurfaceView).move(Direction.Bottom, mvt);
	}

	public void moveTop(View v) {
		((MapView) mGlSurfaceView).move(Direction.Top, mvt);
	}

	public void movePlus(View v) {
		((MapView) mGlSurfaceView).move(Direction.Plus, zoom);
	}

	public void moveMinus(View v) {
		((MapView) mGlSurfaceView).move(Direction.Minus, zoom);
	}
}
