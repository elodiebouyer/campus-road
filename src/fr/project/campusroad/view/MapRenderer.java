package fr.project.campusroad.view;

import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLSurfaceView.Renderer;
import android.util.Log;
import android.widget.Toast;
import fr.project.campusroad.data.Drawable;
import fr.project.campusroad.data.Path;

public class MapRenderer implements Renderer {

	private List<Object> toDraw = null;

	private int mWidth;
	private int mHeight;

	private float X = 0;
	private float Y = 0;
	private float mZoom = 1;

	public MapRenderer(int w, int h) {
		super();
		mWidth = w;
		mHeight = h;
		toDraw = new ArrayList<Object>();
	}

	public void addDrawable(Object o){
		toDraw.add(o);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		gl.glViewport(0, 0, mWidth, mHeight);
		gl.glMatrixMode(GL10.GL_PROJECTION);
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		gl.glViewport(0, 0, width, height);

		gl.glMatrixMode(GL10.GL_PROJECTION);        // set matrix to projection mode
		gl.glLoadIdentity();                        // reset the matrix to its default state
	}

	@Override
	public void onDrawFrame(GL10 gl) {

		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
		gl.glOrthof(-1f, 1f, -1f, 1f, 1f, -1f);
		gl.glClearColor(1f, 1f, 1f, 1);

		gl.glLoadIdentity();

		gl.glTranslatef(X, Y, 0);

		for(Object d:toDraw){
			((Drawable)d).draw(gl, mZoom);
		}

	}

	public void left(float x) {
		X += x;
		if( X < -1*mZoom ) X = -1f*mZoom;
	} 

	public void right(float x) {
		X -= x;
		if( X > 1*mZoom ) X = 1f*mZoom;
	}

	public void bottom(float y) {
		Y += y;
		if( Y < -1*mZoom ) Y = -1f*mZoom;
	}

	public void top(float y ) {
		Y -= y;
		if( Y > 1*mZoom ) Y = 1f*mZoom;
	}


	public void minus(float zoom) {
		mZoom -= zoom;
		if( mZoom < 0.1f ) mZoom = 0.1f;

	}

	public void plus(float zoom) {
		mZoom += zoom;
		if( mZoom > 10.0f ) mZoom = 10.0f;
	}
}

